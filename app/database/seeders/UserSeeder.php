<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'login' => 'admin',
                'password' => Hash::make('12345678'),
            ],
            [
                'login' => 'vadim',
                'password' => Hash::make('12345678'),
            ]
        ];

        foreach ($data as $user) {
            User::on()->create($user);
        }

    }
}
