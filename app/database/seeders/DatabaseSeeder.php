<?php

namespace Database\Seeders;

use App\Models\Actor;
use App\Models\CrossFilmActor;
use App\Models\Film;
use Illuminate\Database\Seeder;
use Illuminate\Support\Arr;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        //Актеры
        $dataActors = [
            [
                'name' => 'Сергей',
                'last_name' => "Тимощук",
                'birthday' => "1997-11-10",
                'country' => "Россия",
            ],
            [
                'name' => 'Иван',
                'last_name' => "Петров",
                'birthday' => "1977-12-12",
                'country' => "США",
            ],
            [
                'name' => 'Джон',
                'last_name' => "Портер",
                'birthday' => "1987-09-09",
                'country' => "Канада",
            ]
        ];

        foreach ($dataActors as $actor) {

            Actor::on()->updateOrCreate($actor, $actor);
        }

        //Фильмы
        $dataFilms = [
            [
                'name' => 'Матрица',
                'description' => "Фильм про нео и его путешествия в матрице",
                'genre' => "Фантастика",
                'year' => "2008"
            ],
            [
                'name' => 'Форсаж',
                'description' => "Фильм про гонки",
                'genre' => "Триллер",
                'year' => "2007"
            ],
            [
                'name' => 'Механик',
                'description' => "Фильм про дикого парня Джона",
                'genre' => "Боевик",
                'year' => "2010"
            ]
        ];

        foreach ($dataFilms as $film) {
            Film::on()->updateOrCreate($film,$film);
        }

        $role = ['Главная', 'Роль второго плана', 'Массовка'];

        for($i = 0; $i <= 5; $i++){

            $data = [
                'film_id' =>  Film::on()->get()->random(1)->first()->id,
                'actor_id' => Actor::on()->get()->random(1)->first()->id,
                'role' =>  Arr::random($role),
                'payment' =>  rand(100000, 300000),
            ];

            CrossFilmActor::on()->create($data);
        }

    }
}
