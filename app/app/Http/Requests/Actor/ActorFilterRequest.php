<?php

namespace App\Http\Requests\Actor;

use App\Models\Actor;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ActorFilterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'params' => [
                'array',
                'nullable'
            ],
            'params.name' => [
                'nullable',
                'string'
            ],
            'params.last_name' => [
                'nullable',
                'string'
            ],
            'params.films|name' => [
                'nullable',
                'string'
            ],
            'params.films|year' => [
                'nullable',
                'date_format:"Y"'
            ],
            'params.films|_payment' => [
                'nullable',
                'string'
            ],
            'params.films|_role' => [
                'nullable',
                'string'
            ],
            'sortBy' => [
                'nullable',
                'string'
            ],
            'direction' => [
                'nullable',
                'string',
                Rule::in(['asc','desc'])
            ],

        ];
    }
}
