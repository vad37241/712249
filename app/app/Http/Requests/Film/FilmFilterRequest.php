<?php

namespace App\Http\Requests\Film;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class FilmFilterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'params' => [
                'array',
                'nullable'
            ],
            'params.name' => [
                'nullable',
                'string'
            ],
            'params.genre' => [
                'nullable',
                'string'
            ],
            'params.actors|name' => [
                'nullable',
                'string'
            ],
            'params.actors|_payment' => [
                'nullable',
                'string'
            ],
            'params.actors|_role' => [
                'nullable',
                'string'
            ],
            'sortBy' => [
                'nullable',
                'string'
            ],
            'direction' => [
                'nullable',
                'string',
                Rule::in(['asc','desc'])
            ]
        ];
    }
}
