<?php

namespace App\Http\Middleware;

use App\Models\AuthLogUser;
use App\Models\User;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthApi
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        $token = $request->header('Authorization');

        if(!empty($token)){

            $exists = AuthLogUser::on()->where('api_token', $token)
                ->where('date_time_token', '>=', now())
                ->first();

            if(!is_null($exists)){

                $user = User::on()->find($exists->user_id);

                Auth::login($user);

                return $next($request);
            }
        }

        return response()->json([
            'result'=>"false",
            'message'=> "Неавторизован",
        ], 401);
    }
}
