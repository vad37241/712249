<?php

namespace App\Http\Controllers;

use App\Http\Requests\Film\FilmFilterRequest;
use App\Repositories\Film\FilmRepositories;

class FilmController extends Controller
{

    /**
     * @var FilmRepositories
     */
    private $filmRepositories;

    public function __construct(FilmRepositories $filmRepositories)
    {

        $this->filmRepositories = $filmRepositories;
    }

    /**
     * Get all
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(FilmFilterRequest $request)
    {
        if(count($request->all()) > 0 ) {

            $request = $request->validated();

            return response()->json([
                'result' => true,
                'data' => $this->filmRepositories->getByFilter($request['params'] ?? [], $request['sortBy'], $request['direction'])
            ]);
        } else{
            return response()->json([
                'result' => true,
                'data' => $this->filmRepositories->getAll()
            ]);
        }

    }

    /**
     * Get by id
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        return response()->json([
            'result' => true,
            'data' => $this->filmRepositories->getById($id)
        ]);
    }
}
