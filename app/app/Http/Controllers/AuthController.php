<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\LoginRequest;
use App\Models\AuthLogUser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class AuthController extends Controller
{
    public function login(LoginRequest $loginRequest)
    {
        $check = Auth::attempt($loginRequest->validated());

        if ($check){
            $token = Str::random(75);

            AuthLogUser::on()->where('user_id', auth()->user()->id)
                ->where('date_time_token', '>', now())
                ->update(['date_time_token' => now()->subMinute()]);

            $auth = AuthLogUser::on()->create([
                'user_id' => auth()->user()->id,
                'api_token' => $token,
                'date_time_token' => now()->addHour(5) // активен на 5 часов
            ]);

            return response()->json([
                "result" => true,
                "data" => [
                    "_token" => $auth['api_token'],
                ]
            ]);

        }else{
            return response()->json([
                "result" => false,
                "message" => "Неверные логин или пароль"
            ]);
        }

    }
}
