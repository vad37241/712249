<?php

namespace App\Http\Controllers;


use App\Http\Requests\Actor\ActorFilterRequest;
use App\Repositories\Actor\ActorRepositories;
use Illuminate\Http\Request;

class ActorController extends Controller
{
    /**
     * @var ActorRepositories
     */
    private ActorRepositories $actorRepositories;

    public function __construct(ActorRepositories $actorRepositories)
    {
        $this->actorRepositories = $actorRepositories;
    }

    /**
     * gel all
     *
     */
    public function index(ActorFilterRequest $request)
    {
        if(count($request->all()) > 0){

            $request = $request->validated();

            return response()->json([
                'return' => true,
                'data' => $this->actorRepositories->getByFilter($request['params'] ?? [], $request['sortBy'] ?? null, $request['direction'] ?? null)
            ]);

        }else{
            return response()->json([
                'return' => true,
                'data' => $this->actorRepositories->getAll()
            ]);
        }
    }

    /**
     * Get by id
     *
     * @param $id
     */
    public function show($id)
    {
        return response()->json([
            'return' => true,
            'data' => $this->actorRepositories->getById($id)
        ]);
    }
}
