<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CrossFilmActor extends Model
{
    use HasFactory;

    protected $fillable = [
        'film_id',
        'actor_id',
        'role',
        'payment'
    ];

    public $timestamps = false;
}
