<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Actor extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'last_name',
        'birthday',
        'country',
    ];

    public $timestamps = true;

    public function films(){
        return $this->belongsToMany(
            Film::class,
            'cross_film_actors',
            'actor_id',
            'film_id')
            ->as('cross')
            ->withPivot(['role', 'payment']);
    }
}
