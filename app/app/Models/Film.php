<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Film extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'description',
        'genre',
        'year',
    ];

    public $timestamps = true;

    public function actors(){
        return $this->belongsToMany(
            Actor::class,
            'cross_film_actors',
            'film_id',
            'actor_id')
            ->as('cross')
            ->withPivot(['role', 'payment']);
    }
}
