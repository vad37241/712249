<?php


namespace App\Repositories\Actor;


use App\Models\Actor;

class ActorRepositories implements ActorInterface
{

    public function getAll()
    {
        return Actor::on()->with(['films'])->get() ?? null;
    }

    public function getById($id)
    {
        return Actor::on()->with(['films'])->find($id) ?? null;
    }

    public function getByFilter($params = [], $sortBy = null, $direction = "asc")
    {
        $actor = Actor::on();

        $actor->with(['films']);

        foreach ($params as $key => $value){
            if (!is_null($value)){
                $hasRelation = explode('|',$key);
                if (count($hasRelation) > 1){
                    $relation = implode('.', array_slice($hasRelation,0, -1));
                    $actor->whereHas($relation, function ($query) use ($hasRelation, $value){

                        $column = end($hasRelation);

                        if(substr($column, 0, 1) != "_"){
                            $query->where($column, "LIKE", "%{$value}%");
                        }else{
                            $column = substr($column,1);
                            $query->whereRaw("`cross_film_actors`.`{$column}` LIKE '%{$value}%' ");
                        }

                    });
                }else{
                    $actor->where($key, "LIKE", "%{$value}%");
                }
            }
        }


        if(!is_null($sortBy)){
            $actor->orderBy($sortBy, $direction);
        }

        $result = $actor->get();

        return $result ?? null;
    }
}
