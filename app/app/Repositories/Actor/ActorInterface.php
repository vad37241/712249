<?php


namespace App\Repositories\Actor;


use App\Models\Actor;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

interface ActorInterface
{
    public function getAll();

    public function getById(int $id);

    public function getByFilter($params = [], $sortBy = null, $direction = "asc");
}
