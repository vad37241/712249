<?php


namespace App\Repositories\Film;

use App\Models\Film;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

interface FilmInterface
{
    public function getAll() ;

    public function getById(int $id) ;

    public function getByFilter($params, $sortBy = null, $direction = "asc");
}
