<?php


namespace App\Repositories\Film;


use App\Models\Film;

class FilmRepositories implements FilmInterface
{
    public function getAll()
    {
        return Film::on()->with(['actors'])->get() ?? null;
    }

    public function getById(int $id)
    {
        return Film::on()->with(['actors'])->find($id) ?? null;
    }

    public function getByFilter($params, $sortBy = null, $direction = "asc")
    {
        $film =Film::on();

        $film->with(['actors']);

        foreach ($params as $key => $value){
            if (!is_null($value)){
                $hasRelation = explode('|',$key);
                if (count($hasRelation) > 1){
                    $relation = implode('.', array_slice($hasRelation,0, -1));
                    $film->whereHas($relation, function ($query) use ($hasRelation, $value){
                        $column = end($hasRelation);

                        if(substr($column, 0, 1) != "_"){
                            $query->where($column, "LIKE", "%{$value}%");
                        }else{
                            $column = substr($column,1);
                            $query->whereRaw("`cross_film_actors`.`{$column}` LIKE '%{$value}%' ");
                        }
                    });
                }else{
                    $film->where($key, "LIKE", "%{$value}%");
                }
            }
        }

        if(!is_null($sortBy)){
            $film->orderBy($sortBy, $direction);
        }

        $result = $film->get();

        return $result ?? null;
    }
}
